import * as WebBrowser from 'expo-web-browser';
import React , {Component} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  TouchableOpacity,
  View,
  TextInput,
  DatePickerAndroid,
  TimePickerAndroid,
  DatePickerIOS,
  Slider
} from 'react-native';

import MapView , { Marker, PROVIDER_GOOGLE }from 'react-native-maps';


import Icon from 'react-native-vector-icons/Ionicons';

import { MonoText } from '../components/StyledText';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import AwesomeDebouncePromise from 'awesome-debounce-promise';


export default class HomeScreen extends Component{

  constructor(props) {
    super(props);
    d=new Date()
    this.state = {
      location: '',
      time:d.getHours()+":"+d.getMinutes(),
      currentLocation:{latitude:47.37861,longitude:8.54,latitudeDelta:0.04,longitudeDelta:0.04},
      errorMessage:'',
      date:new Date(),
      androidDate: `${new Date().getUTCDate()}/${new Date().getUTCMonth() + 1}/${new Date().getUTCFullYear()}`,
      mode: 'date',
      show: false,
      update:"no",
      markers:[],
      loading:false
    };
  }

  async componentDidMount() {
    this._getLocationAsync();
    this.initialiseGarages()
  }

  setDate(newDate) {
    this.setState({ date: newDate });
  }

  setDateAndroid = async () => {
    try {
      const {
        action, year, month, day,
      } = await DatePickerAndroid.open({
      date: new Date(),
      minDate: new Date(),
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ androidDate: `${day}/${month + 1}/${year}` });
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  };

  setTimeAndroid = async () => {
    try {
      d=new Date()
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: d.getHours(),
        minute: d.getMinutes() ,
        is24Hour: false, // Will display '2 PM'
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        // Selected hour (0-23), minute (0-59)
        const m = (minute < 10) ? `0${minute}` : minute;
        const h = (hour < 10) ? `0${hour}` : hour;
        this.setState({ time: `${h}:${m}` });
      }
    } catch ({ code, message }) {
      console.warn('Cannot open time picker', message);
    }
  };

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let currentLocation = await Location.getCurrentPositionAsync({});
    currentLocation={...currentLocation["coords"],...{latitudeDelta:0.01,longitudeDelta:0.01}}
    this.setState({ currentLocation: currentLocation });
  };

  onMapRegionChange = async (loc) => {
    this.setState({
      currentLocation:loc
    })
  }

  notifyChange = async (newLoc,del) => {
    this.setState({
      currentLocation:{
                latitude: newLoc.lat,
                longitude: newLoc.lng,
                latitudeDelta: 0.02,
                longitudeDelta: 0.02
            }
    })
  }

  async getData(){
    var nMarks=[]
    this.setState({
      loading:true,
      markers:this.state.copyOfMarkers
    })
    var convertedDate = this.state.androidDate
    var workingDate = convertedDate.split("/")
    convertedDate=workingDate[2]+"-"+workingDate[1]+"-"+workingDate[0]+'T'+this.state.time+":00"
    let data ;
    passer=false
    await fetch('http://157.245.64.235/api-v1.0/get-nearest-parking/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "location": {
          "latitude": parseFloat(this.state.currentLocation.latitude),
          "longitude":parseFloat(this.state.currentLocation.longitude)
          },
        "time": convertedDate
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      data=responseJson
      for(let i=0; i< data.parkings.length; i++){
        nMarks.push({"latlng":{latitude:data.parkings[i].location.latitude,longitude:data.parkings[i].location.longitude},"title":data.parkings[i].name,"description":data.parkings[i].recommended ? "recommended" : "second option","capacity":50,"route_time":data.parkings[i].route_time})
      }
      passer=true
    })
    .catch((error) => {
      console.error(error);
        });
    if(passer=true){
      this.setState({
        loading:false,
        markers:nMarks
      })      
    }
  }

  resetLocation = async e => {
    this._getLocationAsync()
  }

  initialiseGarages = async e => {
    var nMarks=[]
    await fetch('http://157.245.64.235/api-v1.0/parkings/', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      data=responseJson
    })
    .catch((error) => {
      console.error(error);
        });

    for(let i=0; i< data.parkings.length; i++){
      nMarks.push({"latlng":{latitude:data.parkings[i].location.latitude,longitude:data.parkings[i].location.longitude},"capacity":50})
    }
    console.log(nMarks)
    this.setState({
      markers:nMarks,
      copyOfMarkers:nMarks
    })
  }

  debounceRegionChange = loc => {AwesomeDebouncePromise(this.onMapRegionChange, 10)};

  renderAutocomplete = e => {
    return (
       <GooglePlacesAutocomplete
        placeholder='Search'
        minLength={3} // minimum length of text to search
        autoFocus={false}
        returnKeyType={'search'} // Can be left out for default return key 
        listViewDisplayed={false}    // true/false/undefined
        fetchDetails={true}
        onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
            this.notifyChange(details.geometry.location);
        }}
        query={{
            key: 'AIzaSyB7yEv81SZdLH0IBrzHe3LjSfwxfE2j3WA',
            location: this.state.currentLocation.latitude+","+this.state.currentLocation.longitude,
            radius: "50", //50 km
        }}
        nearbyPlacesAPI='GooglePlacesSearch'
        debounce={1500}

      />
    )
  }

  render () {
    autocompleteComponent=this.renderAutocomplete()
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>

          <View style={styles.getStartedContainer}>

            <Text
              style={styles.userInput}
            >
            Where will you be?
            </Text>
            <View style={styles.searchBar}>
                <Button
                  onPress={() => {this.resetLocation()}}
                  style={{height:10,width:10}}
                  title="Current Location"
                />
               {autocompleteComponent}
            </View>
            <Text
              style={styles.userInput}
            >
            What time will you be there?
            </Text>

          { 
            Platform.OS === 'ios' 
            ? (
                <DatePickerIOS
                  date={this.state.date}
                  onDateChange={this.setDate}
                />
              ) 
            : (
                <View style={styles.AndroidDatePickerWrap}>
                  <TouchableOpacity onPress={() => this.setDateAndroid()}>
                    <View style={styles.HistoryTime}>
                      <Icon name="ios-calendar" size={25} color="rgb(49, 49, 49)" />
                      <Text style={[styles.HistoryTimeText, { fontSize: 16 }]}>
                        {this.state.androidDate}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.setTimeAndroid()}>
                    <View style={styles.HistoryTime}>
                      <Icon name="ios-time" size={25} color="rgb(49, 49, 49)" />
                      <Text style={[styles.HistoryTimeText, { fontSize: 16 }]}>
                        {this.state.time}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )
          }
          </View>
          <Button
            onPress={() => {this.getData()}}
            color={this.state.loading ? 'orange' : ""}
            title={this.state.loading ? "Searching..." : "Find me a spot!"}
          />
          {
            this.state.errorMessage
            ? <Text>{this.state.errorMessage}</Text>
            : <Text></Text>
          }
        </ScrollView>
        <MapView
           style={{height:250}}
           showsUserLocation={true}
           showsMyLocationButton={true}
           region={this.state.currentLocation}
           onRegionChange={(reg) => this.debounceRegionChange(reg)}
           initialRegion={{latitude:47.37861,longitude:8.54,latitudeDelta:0.01,longitudeDelta:0.01}}
        >
        <Marker key="current" coordinate={this.state.currentLocation} />
        {this.state.markers.length!==0 
          ? this.state.markers.map(marker => (
            <Marker key={marker.latlng.latitude+":"+marker.latlng.longitude}
              coordinate={marker.latlng}
              title={marker.title}
              description={marker.description}
              image={marker.description=="recommended"
                ? marker.capacity>=70
                  ? require('../assets/images/garage_issue_130_130.png')
                  : require('../assets/images/garage_empty_130_130.png')
                :require('../assets/images/garage_80_80.png')
              }
              
            />
            ))
          : console.log("empty")
        }
        </MapView>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  title: 'Park.me',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'column',
    backgroundColor: '#fff',
  },
  mainTitle:{
    fontSize:20,
    textAlign:"center"
  },
  AndroidDatePickerWrap:{
    flex:1,
    flexDirection:'row',
    textAlign:'center',
    justifyContent:'space-evenly',
    marginTop:10,
    marginBottom:10
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  userInput:{
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingVertical: 10,
    color:'black',
    flex:1,
    textAlign:'center'
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    flex:1,
    flexDirection:'column',
    alignItems:'stretch',
    justifyContent:'space-between'
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
